
#include <LiquidCrystal_I2C.h>
#include <SoftwareSerial.h>



//Vetor com o pinos utilizados para os botões
 int pins[] = {2,3,4,5,6,7,A0,A1,A2,A3};

LiquidCrystal_I2C lcd(0x3F,2,1,0,4,5,6,7,3, POSITIVE);  // Set the LCD I2C address


//Serial da Balança
SoftwareSerial mySerial(8, 13); // RX, TX

SoftwareSerial SerialDebug(11, 12); // RX, TX

const int rele1 =  10;      //Buzina
const int  rele2 =  9; //Led

int conta1, botao;
int apertou = 0;
int envia = 0;
int SC = 0;
int volume = 0;
int x = 0; //Usada na leitura de botões
String inputString = "";
String aux;
String ope;
String peso;
char ultimoestab, estab;
boolean stringComplete1 = false;

long displayMillis = 0;
long displayinterval = 100;

String Entrada;





void setup() {

//Configura os pinos dos botões habilitando os resistores de pull-up internos
   int i;
   for (i = 0 ; i < 10 ; i++)
            pinMode(pins[i],INPUT_PULLUP); 
   
  pinMode(rele1, OUTPUT);
  pinMode(rele2, OUTPUT);

  Serial.begin(9600);
SerialDebug.begin(9600);
  //mySerial.begin(9600);



  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Working Software");
  lcd.setCursor(0,1);
  lcd.print("Producao 1.0");
  delay(3000);

  lcd.clear();
  lcd.print("Config. balanca");
  lcd.setCursor(0,1);
  lcd.print("Aguarde");
  

//Inicia a configuração da balança
while(!Serial.available()){ 

                            //Aguarda Comunicação com a Serial
                            lcd.setCursor(7,1);
                            lcd.print("       ");
                            delay(200);
                            lcd.setCursor(8,1);
                            lcd.print(".");
                            delay(200);
                            lcd.setCursor(9,1);
                            lcd.print(".");
                            delay(200);
                            lcd.setCursor(10,1);
                            lcd.print(".");
                            delay(300);

                          
                      }

    if(Serial.available()){

                    Entrada = leSerial();

                      SerialDebug.println(Entrada);
                    
                    ope = Entrada.substring(11,12);

                    if (ope == "e"){

                                       RetornaOK("SETUP");

                                       while(!Serial.available()){
                                                                  //Espera
                                                                }
                                                      if(Serial.available()){

                                                        Entrada = "";
                                                        Entrada = leSerial();

                                                                  SerialDebug.println(Entrada);
                                                                  
                                                        int h = Entrada.indexOf("=");
                                                        Entrada = Entrada.substring(h+1);
                                                        h = Entrada.indexOf(" ");
                                                        Entrada = Entrada.substring(0,h);

                                                          SerialDebug.println(Entrada);
                                                          
                                                        mySerial.begin(Entrada.toInt());
                                                        RetornaOK("OK");
                                                        
                                                      }
                                        
                      
                                   
                       
                                    }
                      
                    }
    

  lcd.clear();
  lcd.print("Balanca");
  lcd.setCursor(0,1);
  lcd.print("Configurada");
  delay(1000);

  lcd.clear();
  lcd.print("Volume:");
  lcd.setCursor(10,0);
  lcd.print(volume);
   

}

void loop() {
  lebalanca();
  botao = lerBotoes();
  delay(1);

    if(Serial.available()){

      Entrada = leSerial();

       


      ope = Entrada.substring(11,12);

      if ( (ope == "e") & (envia == 1)){

                
              
               RetornaOK(aux);

                      lcd.setCursor(0,0);
                      lcd.print("                ");
                      lcd.setCursor(0,0);
                      lcd.print("Validando..");
                     // delay(200);

                while(!Serial.available()){

                      
                  }

                   if(Serial.available()){

                    Entrada = "";
                    Entrada = leSerial();

                    SerialDebug.println(Entrada);

                    int h = Entrada.indexOf("=");
                     Entrada = Entrada.substring(h+1);
                     h = Entrada.indexOf(" ");
                     Entrada = Entrada.substring(0,h);

                          if (Entrada == "Erro"){

                                   lcd.setCursor(0,0);
                                   lcd.print("                ");
                                   lcd.setCursor(0,0);
                                   lcd.print("FORA DA FAIXA");
                                   digitalWrite(rele1, HIGH);
                                   delay(500);
                                   digitalWrite(rele1, LOW);
                                   RetornaOK("OK");
                                   lcd.setCursor(0,0);
                                   lcd.print("                ");
                                   lcd.setCursor(0,0);
                                   lcd.print("Volume:");
                                   lcd.setCursor(10,0);
                                   lcd.print(volume); 
                                   Entrada = "";                   
                                   envia = 0;
                          
                          }
                          else if (Entrada == "Estoque"){

                                   lcd.setCursor(0,0);
                                   lcd.print("                ");
                                   lcd.setCursor(0,0);
                                   lcd.print("SEM ESTOQUE");
                                   digitalWrite(rele1, HIGH);
                                   delay(500);
                                   digitalWrite(rele1, LOW);
                                   RetornaOK("OK");
                                   lcd.setCursor(0,0);
                                   lcd.print("                ");
                                   lcd.setCursor(0,0);
                                   lcd.print("Volume:");
                                   lcd.setCursor(10,0);
                                   lcd.print(volume); 
                                   Entrada = "";                   
                                   envia = 0;
                          
                          }
                          else{          
                                   volume = volume +1;           
                                   lcd.setCursor(0,0);
                                   lcd.print("                ");
                                   lcd.setCursor(0,0);
                                   lcd.print(Entrada);
                                   digitalWrite(rele2, HIGH);
                                   delay(500);
                                   digitalWrite(rele2, LOW);
                                   delay(1000);
                                   RetornaOK("OK");
                                   lcd.setCursor(0,0);
                                   lcd.print("                ");
                                   lcd.setCursor(0,0);
                                   lcd.print("Volume:");
                                   lcd.setCursor(10,0);
                                   lcd.print(volume);
                                   envia = 0;
                                   
                                   
                                   
                          }
                       }
             
                                 
         }
         else{
                if (SC == 1){
                  RetornaOK("*SC*");
                }
                else{
                
                RetornaOK("ZERO");
                }
             }


      

      
    }// Fim checa serial


    
if ((botao != 0) && (apertou == 0) ){

    aux = "";                                               
    aux = botao;
    aux += ";";
      /*
        while (inputString == ""){
                                  Serial.println("ops");
                                  lebalanca();
                                 }    
        */
    aux += peso;
    //Serial.println(inputString);
    apertou = 1;
    envia = 1;
  
}
    
  
  

}// fim Loop


//Funções

void lebalanca () {
  int comunica1 = 0;

  while (mySerial.available()) {
    comunica1 = 1;
    conta1 = 0;
    char inChar = (char)mySerial.read();
    byte g = inChar;

    if ((inChar != 2) && (inChar != 1) && (inChar != 32)) {
      if ((inChar != 69) && (inChar != 73)) {
        inputString += inChar;
        // peso1[o] = inChar;
        //    Serial.print(inChar);
        //Serial.println(inputString);
      }
      else {
        if ((inChar == 69) || (inChar == 73)) {
          ultimoestab = estab;
          estab = inChar;
        }
      }
    }
    if (inChar == 1) {
      stringComplete1 = true;
      break;
    }
    delay(5);
  }

  if (stringComplete1) {
    comunica1 = 1;
    
    peso = inputString;
    //delay(50);

    //Serial.println(inputString);
    //Serial.println(peso);
    
    if (millis() - displayMillis > displayinterval) {
      displayMillis = millis();
      lcd.setCursor(7,0);
    lcd.print("        ");
    lcd.setCursor(0,0);
    lcd.print("Volume:");
    lcd.setCursor(10,0);
    lcd.print(volume);
      lcd.setCursor(0, 1);
      lcd.print("Peso:                      ");
      int i, y;
      int w = peso.toFloat();
      //int w = inputString.toFloat();
      if ((w >= 0) && (w < 10)) {
        i = 7;
      }
      if ((w >= 10) && (w < 100)) {
        i = 6;
      }
      if ((w >= 100) && (w < 1000)) {
        i = 5;
      }
      lcd.setCursor(i, 1);
      lcd.print(peso.toFloat());
      //lcd.print(inputString.toFloat());
      lcd.setCursor(12, 1);
      lcd.print("kg");
      lcd.setCursor(15, 1);
      lcd.print(estab);
     }

    
    
    inputString = "";
    stringComplete1 = false;
    delay(1);
    apertou = 0;
    SC = 0;
    
  } 

  conta1++;
  if ((comunica1 == 0) && (conta1 > 10000)) {

     SC = 1; 
     apertou = 1;
     conta1++;     
     
    lcd.setCursor(0, 0);
    lcd.print("SEM COMUNICACAO      ");
    lcd.setCursor(0, 1);
    lcd.print("COM A BALANCA        ");    

    inputString = "";
    conta1 = 0;
  }
}//Fim LeBalanca


//Função para leitura dos botões
int lerBotoes() {

  x = 0;

  for ( int i = 0; i < 10 ; i++){

    if ( digitalRead(pins[i]) == LOW){

                                      x = i;  
                                      return x+1;
                                      
                                      break;
      
                                     }
    
                                }                                     

                     return x;

                } //Fim lerBotoes

void RetornaOK(String strValImp) {
  //Responde com OK    
  String strDadImp = "";

  //Serial.println("ENTROU NO OK");
  Serial.println("HTTP/1.1 200 OK");
  Serial.println("Content-Type: text/html");
  Serial.println("Connection: close");

  strDadImp = "<!DOCTYPE HTML>";
  strDadImp += "<html>";
  strDadImp += strValImp;
  strDadImp += "</html>";

  Serial.print("Content-Length: ");
  Serial.print(strDadImp.length());
  Serial.print("\r\n\r\n");
  Serial.println(strDadImp);
  //apertou = 0;
  botao =0;
}


String leSerial(){

  String strlida = "";
  //String strlida2 = "";
  //strlida = Serial.readStringUntil('\n') ;
  
  while(Serial.available()){


            //strlida = Serial.readStringUntil('1') ;

            //Serial.flush();
            //}
              
            char bp = (char)(Serial.read());

                   strlida += bp;
                
             if( (bp == '\n') ){

              while(Serial.available()){
                      Serial.read();
                      delay(3);
              }

              
              
                           
             }
                
             
       delay(3);  //Somente pra estabillizar a leitura da serial
  }

  return strlida;
  
}// Fim leSerial
