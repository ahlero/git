//--- Inclue as bibliotecas do projeto ---
#include <LiquidCrystal.h>
#include <MUX74HC4067.h>
#include <SoftwareSerial.h>


//Configura os pinos dos paineis que tem os pinos ligados direto.
LiquidCrystal lcd(16, 17, 9, 11, 12, 13);


MUX74HC4067 mux(3, 4, 5, 6, 7);


SoftwareSerial SerialDebug(19, 18); // RX, TX
//Serial da Balança
SoftwareSerial mySerial(8, 10); // RX, TX

const int led_verde =  A0;      //Led
const int  led_vermelho =  A1; //Buzina

int conta1, botao;
int apertou = 0;
int envia = 0;
int SC = 0;
int volume = 0;
int x = 0; //Usada na leitura de botões
String inputString = "";
String aux;
String ope;
String peso;
char ultimoestab, estab;
boolean stringComplete1 = false;

signed long displayMillis = 0;
signed long displayinterval = 100;

String Entrada;
String Debug;




void setup() {
  pinMode(led_verde, OUTPUT);
  pinMode(led_vermelho, OUTPUT);

  SerialDebug.begin(9600);
  Serial.begin(9600);

  //mySerial.begin(9600);

  mux.signalPin(2, INPUT, DIGITAL);

  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Working Software");
  lcd.setCursor(0, 1);
  lcd.print("Producao 1.0");
  delay(3000);

  lcd.clear();
  lcd.print("Config. balanca");
  lcd.setCursor(0, 1);
  lcd.print("Aguarde...");



  while (!Serial.available()) {

    //Aguarda Comunicação com a Serial
  }

  if (Serial.available()) {

    Entrada = leSerial();
    ope = Entrada.substring(11, 12);

    if (ope == "e") {

      RetornaOK("SETUP");

      while (!Serial.available()) {
        //Espera
      }
      if (Serial.available()) {

        Entrada = "";
        Entrada = leSerial();

        int h = Entrada.indexOf("=");
        Entrada = Entrada.substring(h + 1);
        h = Entrada.indexOf(" ");
        Entrada = Entrada.substring(0, h);

        mySerial.begin(Entrada.toInt());
        RetornaOK("OK");

      }




    }

  }



  lcd.clear();

  lcd.print("Volume:");
  lcd.setCursor(10, 0);
  lcd.print(volume);


}

void loop() {
  lebalanca();
  botao = leiturabotoes ();
  delay(1);

  if (Serial.available()) {                      //Verifica contantemente se existe algo na serial

    Entrada = leSerial();                        //Caso haja algo na serial busca pelo operador "e" ---


    ope = Entrada.substring(11, 12);

    if ( (ope == "e") & (envia == 1)) {          //Se o operador for "e" e algum botão for apertado...



        RetornaOK(aux);                          //Envia para o servidor o botão apertado e o peso

        Debug = aux;
        Debug += '\r';
        Debug += '\n';
      
        lcd.setCursor(0, 0);                     //
        lcd.print("                ");           //
        lcd.setCursor(0, 0);                     // Escvreve "Validando" no display do painel 
        lcd.print("Validando...");               //
       

      while (!Serial.available()) {              //Aguarda nova responta do servidor

        
      }

      if (Serial.available()) {                  // Se o servidor responder

        Entrada = "";                            // Limpa a variavel entrada

        Debug += Entrada;
        Debug += '\r';
        Debug += '\n';
               
        Entrada = leSerial();                    // Armazena o que for recebido do srvidor

        Debug += Entrada;
        Debug += '\r';
        Debug += '\n';

        int h = Entrada.indexOf("=");
        Entrada = Entrada.substring(h + 1);
        h = Entrada.indexOf(" ");
        Entrada = Entrada.substring(0, h);

        Debug += Entrada;
        Debug += '\r';
        Debug += '\n';

        SerialDebug.println(Debug);

        if (Entrada == "Erro") {                    //Em caso do nome ser "Erro" executa os comando abaixo

          lcd.setCursor(0, 0);
          lcd.print("                ");
          lcd.setCursor(0, 0);
          lcd.print("FORA DA FAIXA");
          digitalWrite(led_vermelho, HIGH);
          delay(500);
          digitalWrite(led_vermelho, LOW);
          RetornaOK("OK");
          lcd.setCursor(0, 0);
          lcd.print("                ");
          lcd.setCursor(0, 0);
          lcd.print("Volume:");
          lcd.setCursor(10, 0);
          lcd.print(volume);
          Entrada = "";
          envia = 0;

        }
        else if (Entrada == "Estoque") {           //Em caso do nome ser "Estoque" executa os comando abaixo 

          lcd.setCursor(0, 0);
          lcd.print("                ");
          lcd.setCursor(0, 0);
          lcd.print("SEM ESTOQUE");
          digitalWrite(led_vermelho, HIGH);
          delay(500);
          digitalWrite(led_vermelho, LOW);
          RetornaOK("OK");
          lcd.setCursor(0, 0);
          lcd.print("                ");
          lcd.setCursor(0, 0);
          lcd.print("Volume:");
          lcd.setCursor(10, 0);
          lcd.print(volume);
          Entrada = "";
          envia = 0;

        }
        else {                                        //Senão escreve o nome recebido no display
          volume = volume + 1;
          lcd.setCursor(0, 0);
          lcd.print("                ");
          lcd.setCursor(0, 0);
          lcd.print(Entrada);
          digitalWrite(led_verde, HIGH);
          delay(500);
          digitalWrite(led_verde, LOW);
          delay(1000);
          RetornaOK("OK");
          lcd.setCursor(0, 0);
          lcd.print("                ");
          lcd.setCursor(0, 0);
          lcd.print("Volume:");
          lcd.setCursor(10, 0);
          lcd.print(volume);
          envia = 0;


        }
      }


    }
    else {
      if (SC == 1) {
        RetornaOK("*SC*");                         //Para o caso de perder a comunicação com a balança, envia "SC*" para o servidor
      }
      else {

        RetornaOK("ZERO");
      }
    }




    //int h = teste.indexOf("=");
    //teste = teste.substring(h+1);
    //h = teste.indexOf(" ");
    //teste = teste.substring(0,h);
    //lcd.setCursor(0,0);
    //lcd.print("                ");
    //lcd.setCursor(0,0);
    //lcd.print(teste);
    //Serial.println(teste);
    //RetornaOK(teste);
  }



  if ((botao != 0) && (apertou == 0) ) {

    aux = "";
    aux = botao;
    aux += ";";
    /*
      while (inputString == ""){
                                Serial.println("ops");
                                lebalanca();
                               }
    */
    aux += peso;
    //Serial.println(inputString);
    apertou = 1;
    envia = 1;

  }




}// fim Loop


//Funções

void lebalanca () {
  int comunica1 = 0;

  while (mySerial.available()) {
    comunica1 = 1;
    conta1 = 0;
    char inChar = (char)mySerial.read();
    //byte g = inChar;

    if ((inChar != 2) && (inChar != 1) && (inChar != 32)) {
      if ((inChar != 69) && (inChar != 73)) {
        inputString += inChar;
        // peso1[o] = inChar;
        //    Serial.print(inChar);
        //Serial.println(inputString);
      }
      else {
        if ((inChar == 69) || (inChar == 73)) {
          ultimoestab = estab;
          estab = inChar;
        }
      }
    }
    if (inChar == 2) {
      stringComplete1 = true;
      break;
    }
    delay(5);
  }

  if (stringComplete1) {
    comunica1 = 1;

    peso = inputString;
    //delay(50);

    //Serial.println(inputString);
    //Serial.println(peso);

    if (millis() - displayMillis > displayinterval) {
      displayMillis = millis();
      lcd.setCursor(0, 1);
      lcd.print("Peso:                      ");
      int i;
      int w = peso.toFloat();
      //int w = inputString.toFloat();
      if ((w >= 0) && (w < 10)) {
        i = 7;
      }
      if ((w >= 10) && (w < 100)) {
        i = 6;
      }
      if ((w >= 100) && (w < 1000)) {
        i = 5;
      }
      lcd.setCursor(i, 1);
      lcd.print(peso.toFloat());
      //lcd.print(inputString.toFloat());
      lcd.setCursor(12, 1);
      lcd.print("kg");
      lcd.setCursor(15, 1);
      lcd.print(estab);
    }


    lcd.setCursor(0, 0);
    lcd.print("                ");
    lcd.setCursor(0, 0);
    lcd.print("Volume:");
    lcd.setCursor(10, 0);
    lcd.print(volume);


    inputString = "";
    stringComplete1 = false;
    delay(1);
    apertou = 0;
    SC = 0;

  }

  conta1++;
  if ((comunica1 == 0) && (conta1 > 10000)) {

    SC = 1;
    apertou = 1;
    conta1++;

    lcd.setCursor(0, 0);
    lcd.print("SEM COMUNICACAO      ");
    lcd.setCursor(0, 1);
    lcd.print("COM A BALANCA        ");

    inputString = "";
    conta1 = 0;
  }
}//Fim LeBalanca

int leiturabotoes () {
  x = 0;
  //int btreset = -1;
  byte data;

  for (byte i = 0; i < 10; ++i) {
    data = mux.read(i);

    if ( data == LOW ) {
      x = i;
      return x + 1;
      break;
    }
    else {
      x = 0;
    }
  }

  return x;
}//Fim leiturabotoes

void RetornaOK(String strValImp) {
  //Responde com OK
  String strDadImp = "";

  //Serial.println("ENTROU NO OK");
  Serial.println("HTTP/1.1 200 OK");
  Serial.println("Content-Type: text/html");
  Serial.println("Connection: close");

  strDadImp = "<!DOCTYPE HTML>";
  strDadImp += "<html>";
  strDadImp += strValImp;
  strDadImp += "</html>";

  Serial.print("Content-Length: ");
  Serial.print(strDadImp.length());
  Serial.print("\r\n\r\n");
  Serial.println(strDadImp);
  //apertou = 0;
  botao = 0;
}


String leSerial() {

  String strlida = "";

  while(Serial.available()){

                         
            char bp = (char)(Serial.read());

                   strlida += bp;
                
             if( (bp == '\n') ){

              while(Serial.available()){
                      Serial.read();
                      delay(3);
                     }
             
             }
       delay(3);  //Somente pra estabillizar a leitura da serial
  }

  return strlida;
}// Fim leSerial









